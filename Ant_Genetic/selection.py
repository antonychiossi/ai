
import time
import random


class Selection:

    def __init__(self, population):
        self.population = population

    # montecarlo
    def roulette_selection(self):

        fs = [chrm.get_fitness() for chrm in self.population]
        sum_fs = sum(fs)
        max_fs = max(fs)
        min_fs = min(fs)

        random.seed(int(time.time()*1000.0))
        p = random.random()*sum_fs
        t = max_fs + min_fs
        choosen_chrm = self.population[0]

        for chrm in self.population:

            p -= (t - chrm.get_fitness())

            if p < 0:
                choosen_chrm = chrm
                break

        return choosen_chrm
