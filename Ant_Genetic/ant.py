#!/usr/bin/env python
# -*- coding: UTF-8 -*-

import argparse
import time
from cleaner import Cleaner
from collections import namedtuple
from chromosome import Chromosome
import random
import math
import multiprocessing
import concurrent.futures

N_CPU = multiprocessing.cpu_count()
random.seed(int(time.time()*1000.0))
couple = namedtuple('Couple', 'sol cost')


def montecarlo(vals):

        sum_vals = sum(vals)
        random.seed(int(time.time()*1000.0))
        point = random.random()*sum_vals
        sum_vals = 0

        # print(point)
        for i, v in enumerate(vals):
            sum_vals += v
            if sum_vals >= point:
                return i


def build_sol():
    values = [0 for _ in range(Cleaner.n_items)]
    taken = [0 for _ in range(Cleaner.n_items)]
    sol = [0 for _ in range(Cleaner.n_items)]

    for i in range(Cleaner.n_items):

        for j in range(Cleaner.n_items):
            if taken[j] == 1:
                # probabilità di scegliere elem è 0 perché è gia nella sol.
                values[j] = 0
            else:
                values[j] = math.pow(tau[i][j], alpha) * math.pow(eta[i][j], beta)
            # print(values)

        choosen = montecarlo(values)
        sol[i] = choosen
        taken[choosen] = 1

    # print(sol)
    chrm_sol = Chromosome(sol, Chromosome.calc_fitness(sol))
    if random.random() > 0.8:
        chrm_sol.compute_local_search()
    sol = chrm_sol.get_genes()
    return sol


def task(idx):
    partial_ant_sols = []
    iters = int(n_ants/N_CPU)
    # odd_check = n_ants == N_CPU * iters
    # remainder = int((n_ants - iters * N_CPU) / 2)
    # iters = iters + remainder if idx == N_CPU-1 and not odd_check else iters

    local_best_solution = {"sol": [0 for _ in range(Cleaner.n_items)], "cost": 9999}

    start, end = iters * idx, (iters * (idx+1)) - 1
    remainder = n_ants - (iters * N_CPU)
    if remainder != 0 and idx == N_CPU-1:
        end += remainder
    # print(idx, iters, start, end)

    try:
        for ant in range(start, end+1):
            sol = build_sol()

            fitness = Chromosome.calc_fitness(sol)
            if fitness < local_best_solution["cost"]:
                # print(local_best_solution.sol)
                local_best_solution["sol"] = sol
                local_best_solution["cost"] = fitness

            partial_ant_sols.append(sol)
            # print(ant_sols)
    except Exception as e:
        print(e)

    print(idx, len(partial_ant_sols))
    return {"partial_ant_sols": partial_ant_sols, "local_sols": local_best_solution}


if __name__ == '__main__':
    parser = argparse.ArgumentParser(description='Process some integers.')
    parser.add_argument('-f', metavar='FILE', type=str, required=True, help='input file to be parsed')
    parser.add_argument('-i', metavar='ITERATIONS', type=int, required=True, help='number of iterations')
    parser.add_argument('-n', metavar='N', type=int, required=True, help='number of ants')
    parser.add_argument('-ncpu', metavar='N', type=int, help='number of individuals')

    args = parser.parse_args()

    istance_file = args.f
    iterations = int(args.i)
    if args.ncpu is not None:
        N_CPU = int(args.ncpu)
    start_time = time.time()

    # get costs's matrix
    cleaner = Cleaner(istance_file).parse()
    costs = Cleaner.matrix

    # global solution
    best_solution = couple([0 for x in range(Cleaner.n_items)], 9999)

    # initialization
    tau0 = 4 * Cleaner.max_cost
    alpha = 2 #0.5
    beta = 2 #0.5
    rho = 0.9
    n_ants = args.n

    tau = [[0 for x in range(Cleaner.n_items)] for y in range(Cleaner.n_items)]
    eta = [[0 for x in range(Cleaner.n_items)] for y in range(Cleaner.n_items)]

    for i in range(Cleaner.n_items):
        for j in range(Cleaner.n_items):
            tau[i][j] = tau0
            eta[i][j] = Cleaner.max_cost - costs[i][j]

    # ---------------------------------------- ALGORITHM starts ------------------------------------------

    # ant_sols = [[0 for _ in range(Cleaner.n_items)] for x in range(n_ants)]
    for k in range(iterations):

        ant_sols = []
        with concurrent.futures.ProcessPoolExecutor(max_workers=5) as executor:

            future_to_url = {executor.submit(task, i): i for i in range(N_CPU)}

            for future in concurrent.futures.as_completed(future_to_url):
                url = future_to_url[future]
                try:
                    data = future.result()
                except Exception as exc:
                    print('%r generated an exception: %s' % (url, exc))
                else:
                    # print(future)
                    # print(data)
                    # print(len(ant_sols))
                    if data["local_sols"]["cost"] < best_solution.cost:
                        best_solution = best_solution._replace(sol=data["local_sols"]["sol"])
                        best_solution = best_solution._replace(cost=data["local_sols"]["cost"])
                    ant_sols.extend(data["partial_ant_sols"])
                    # print(len(ant_sols))

        # for ia, a in enumerate(ant_sols):
        #     print(ia, len(a))
        #     print(a)

        # trace evaporation
        for i in range(Cleaner.n_items):
            for j in range(Cleaner.n_items):
                tau[i][j] *= rho

        # traces update
        for a in range(n_ants):
            for s in range(Cleaner.n_items):
                # print(ant_sols[a][s])
                tau[s][ant_sols[a][s]] += (1.0 / costs[s][ant_sols[a][s]])

        # print(best_solution.sol)

    print("###################")
    print(best_solution)
    print(best_solution.cost)
    print("--- %s seconds ---" % (time.time() - start_time))
    # get_best(p.get_individuals()[:])
    # print(Chromosome.calc_fitness(best_solution))







