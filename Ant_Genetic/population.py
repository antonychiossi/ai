
import random
import time
from chromosome import Chromosome
from cleaner import Cleaner


class Population:
    def __init__(self):
        self.individuals = None

    def get_individuals(self):
        return self.individuals

    def set_individuals(self, individuals):
        self.individuals = individuals

    def popolate(self, howmany, costs):
        self.individuals = [self.__randomize(Cleaner.n_items, costs) for _ in range(howmany)]

    @staticmethod
    def __randomize(howmany, costs):

        lst = list(reversed(range(howmany)))
        random.seed(int(time.time()*1000.0))
        # for _ in lst:
        #     a = random.randint(0, howmany-1)
        #     b = random.randint(0, howmany-1)
        #     lst[a], lst[b] = lst[b], lst[a]
        random.shuffle(lst)

        # fitness = 0
        # for i, j in enumerate(lst):
        #     fitness += costs[i][j]
        fitness = Chromosome.calc_fitness(lst)

        chrm = Chromosome(lst, fitness)
        return chrm


