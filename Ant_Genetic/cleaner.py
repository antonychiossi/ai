#!/usr/bin/env python
# -*- coding: UTF-8 -*-

import sys


class Cleaner:

    n_items = None
    matrix = None
    path = None
    max_cost = 0

    def __init__(self, istance_path):
        Cleaner.path = istance_path
        # self.matrix = None
        # self.n_items = None

    # def get_costs(self):
    #     return self.matrix
    #
    # def get_nitems(self):
    #     return self.n_items

    @staticmethod
    def parse():
        try:
            with open(Cleaner.path) as infile:
                i, j = 0, 0
                matrix = None
                for idx, line in enumerate(infile):

                    if idx == 0:
                        n = int(line.replace(" ", ""))
                        Cleaner.n_items = n
                        matrix = [[0 for _ in range(n)] for _ in range(n)]

                    if idx > 0 and i < n:
                        # print(line)
                        # costs = replace('\n', '')
                        costs = [int(s) for s in line[1:].replace('\n', '').split(" ") if s.isdigit()]
                        # costs = line[1:].replace('\n', '').split(" ")
                        # print(costs)
                        for cost in costs:
                            if j < n:
                                matrix[i][j] = int(cost)
                                Cleaner.max_cost = int(cost) if int(cost) > Cleaner.max_cost else Cleaner.max_cost
                                if j+1 == n:
                                    i += 1
                                    j = 0
                                else:
                                    j += 1
                Cleaner.matrix = matrix

                # k = 0
                # for i in range(n):
                #     print(matrix[i])
                #     k += len(matrix[i])
                # print(k)

        except Exception as e:
            print(e)
            sys.exit()






