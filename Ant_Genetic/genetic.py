#!/usr/bin/env python
# -*- coding: UTF-8 -*-

import sys
import argparse
import time
from cleaner import Cleaner
from population import Population
from selection import Selection
from crossover import Crossover
from chromosome import Chromosome
import multiprocessing
import concurrent.futures
import random
import threading

N_CPU = multiprocessing.cpu_count()
random.seed(int(time.time()*1000.0))


def get_best(pop):
        b = 9999
        for chrm in pop:
            s = Chromosome.calc_fitness(chrm.get_genes())
            b = s if s < b else b
        print("@@@ CHECK : %d" % b)


def task(name):
    global p
    local_new_population = []
    local_best_fitness = 99999
    local_best_solution = []

    iters = int(n_individuals/(N_CPU*2))
    odd_check = n_individuals == N_CPU * iters * 2
    remainder = int((n_individuals - iters * N_CPU * 2) / 2)
    iters = iters + remainder if name == N_CPU-1 and not odd_check else iters
    # print("%s %d" % (str(name), iters))
    for i in range(iters):
        s = Selection(p.get_individuals())
        parent1 = s.roulette_selection()
        parent2 = s.roulette_selection()

        couple = Crossover(parent1, parent2).two_point() #.single_point()

        if random.random() > 0.9:
            couple.child1.mutation(random.randint(0, Cleaner.n_items-1), random.randint(0, Cleaner.n_items-1))
        if random.random() > 0.9:
            couple.child2.mutation(random.randint(0, Cleaner.n_items-1), random.randint(0, Cleaner.n_items-1))

        # Local Search
        if random.random() > 0.8:
            couple.child1.compute_local_search()
        if random.random() > 0.8:
            couple.child2.compute_local_search()

        # update best fitness & population
        fitness_child1, fitness_child2 = couple.child1.get_fitness(), couple.child2.get_fitness()

        if fitness_child1 < local_best_fitness:
            local_best_fitness = fitness_child1
            local_best_solution = couple.child1.get_genes()

        if fitness_child2 < local_best_fitness:
            local_best_fitness = fitness_child2
            local_best_solution = couple.child2.get_genes()

        local_new_population.append(couple.child1)
        local_new_population.append(couple.child2)

    return {"population": local_new_population, "solution": local_best_solution, "fitness": local_best_fitness}


if __name__ == '__main__':

    parser = argparse.ArgumentParser(description='Process some integers.')
    parser.add_argument('-f', metavar='FILE', type=str, required=True, help='input file to be parsed')
    parser.add_argument('-i', metavar='ITERATIONS', type=int, required=True, help='number of iterations')
    parser.add_argument('-n', metavar='N', type=int, required=True, help='number of individuals')
    parser.add_argument('-ncpu', metavar='N', type=int, help='number of individuals')

    args = parser.parse_args()

    istance_file = args.f
    iterations = int(args.i)
    n_individuals = int(args.n)
    if args.ncpu is not None:
        N_CPU = int(args.ncpu)
    start_time = time.time()

    # get costs's matrix
    cleaner = Cleaner(istance_file).parse()
    costs = Cleaner.matrix

    # create a popolation
    p = Population()
    p.popolate(n_individuals, costs)
    best_fitness = 99999
    best_solution = []

    for k in range(iterations):

        new_population = []
        with concurrent.futures.ProcessPoolExecutor(max_workers=5) as executor:
            future_to_url = {executor.submit(task, i): i for i in range(N_CPU)}
            for future in concurrent.futures.as_completed(future_to_url):
                url = future_to_url[future]
                try:
                    data = future.result()
                except Exception as exc:
                    print('%r generated an exception: %s' % (url, exc))
                else:
                    # print(data["solution"])
                    if data["fitness"] < best_fitness:
                        best_fitness = data["fitness"]
                        best_solution = data["solution"]
                    new_population.extend(data["population"][:])

        p.set_individuals(new_population[:])
        # print('%d - BSolution is: %d' % (k, best_fitness))
        # print('%d - |population| is: %d' % (k, len(p.get_individuals())))
        # get_best(p.get_individuals()[:])

    print("###################")
    print(best_solution)
    print(best_fitness)
    print("--- %s seconds ---" % (time.time() - start_time))
    # get_best(p.get_individuals()[:])
    print(Chromosome.calc_fitness(best_solution))







