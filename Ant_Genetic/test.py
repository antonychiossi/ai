from crossover import Crossover
from chromosome import Chromosome
import random
import time


def get_first_child_2p(start, end):
        part = c2[start:end]
        check_part = c1[:start] + c1[end:]
        print "check :" + str(check_part)
        child = c1[:start] + [x for x in part if x not in check_part] + c1[end:]
        print "1 :" + str(child)
        child = child + [x for x in range(len(c1)) if x not in child]
        print "child :" + str(child)

random.seed(int(time.time()*1000.0))
lst = range(10)
random.shuffle(lst)
c1 = lst

random.seed(int(time.time()*10.0))
lst = range(10)
random.shuffle(lst)
c2 = lst

l = len(c1)

first_cutpoint = random.randint(0, int(l/2))
second_cutpoint = random.randint(int(l/2), l - 1)
if second_cutpoint < first_cutpoint:
    tmp = first_cutpoint
    first_cutpoint = second_cutpoint
    second_cutpoint = tmp

print c1, c2
print first_cutpoint, second_cutpoint
get_first_child_2p(first_cutpoint, second_cutpoint)

# print c1, c2, get_first_child_2p(first_cutpoint, second_cutpoint)


