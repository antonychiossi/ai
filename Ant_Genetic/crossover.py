
import random
import time
import collections
from chromosome import Chromosome
from cleaner import Cleaner


class Crossover:

    def __init__(self, c1, c2):
        self.chrm1 = c1.get_genes()
        self.chrm2 = c2.get_genes()
        # print Cleaner.n_items

    def single_point(self):

        """

        :rtype: couple(chromosome1, chromosome2)
        """
        random.seed(int(time.time()*1000.0))
        # random.shuffle(self.chrm1)
        # random.shuffle(self.chrm2)

        l = len(self.chrm1)

        cutpoint = random.randint(0, l - 1)

        first_child = self.__get_first_child(cutpoint, l)
        second_child = self.__get_second_child(cutpoint, l)

        couple = collections.namedtuple('Couple', ['child1', 'child2'])
        return couple(first_child, second_child)

    def two_point(self):

        """

        :rtype: couple(chromosome1, chromosome2)
        """
        random.seed(int(time.time()*1000.0))
        # random.shuffle(self.chrm1)
        # random.shuffle(self.chrm2)

        l = len(self.chrm1)

        first_cutpoint = random.randint(0, int(l/2)-1)
        second_cutpoint = random.randint(int(l/2), l - 1)
        if second_cutpoint < first_cutpoint:
            tmp = first_cutpoint
            first_cutpoint = second_cutpoint
            second_cutpoint = tmp

        first_child = self.__get_first_child_2p(first_cutpoint, second_cutpoint, l)
        second_child = self.__get_first_child_2p(first_cutpoint, second_cutpoint, l)

        couple = collections.namedtuple('Couple', ['child1', 'child2'])
        return couple(first_child, second_child)

    def __get_first_child_2p(self, start, end, l):
        part = self.chrm2[start:end]
        check_part = self.chrm1[:start] + self.chrm1[end:]
        child = self.chrm1[:start] + [x for x in part if x not in check_part] + self.chrm1[end:]
        child = child + [x for x in range(l) if x not in child]
        return Chromosome(child, Chromosome.calc_fitness(child))

    def __get_second_child_2p(self, start, end, l):
        part = self.chrm1[start:end]
        check_part = self.chrm2[:start] + self.chrm2[end:]
        child = self.chrm2[:start] + [x for x in part if x not in check_part] + self.chrm2[end:]
        child = child + [x for x in range(l) if x not in child]
        return Chromosome(child, Chromosome.calc_fitness(child))

    def __get_first_child(self, cutpoint, l):
        half1 = self.chrm1[:cutpoint]
        child = half1 + [x for x in self.chrm2[cutpoint:] if x not in half1]
        child = child + [x for x in range(l) if x not in child]
        return Chromosome(child, Chromosome.calc_fitness(child))

    def __get_second_child(self, cutpoint, l):
        half2 = self.chrm2[cutpoint:]
        child = half2 + [x for x in self.chrm1[:cutpoint] if x not in half2]
        child = child + [x for x in range(l) if x not in child]
        return Chromosome(child, Chromosome.calc_fitness(child))


