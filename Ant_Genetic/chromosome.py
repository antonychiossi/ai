from cleaner import Cleaner
import random


class Chromosome:
    def __init__(self, genes, fitness):
        self.genes = genes
        self.fitness = fitness

    def get_genes(self):
        return self.genes

    def get_fitness(self):
        return self.fitness

    def compute_local_search(self):
        is_better = True
        while is_better:
            is_better = self.__local_search()

    def __local_search(self):
        # point = random.randint(0, Cleaner.n_items/2-1)
        # n_items = range(point, Cleaner.n_items)
        n_items = range(Cleaner.n_items)
        for i in n_items:
                for j in n_items:
                    if i != j:

                        # print "old: ", self.get_fitness()
                        new_fitness = self.__smart_calc_fitness(i, j)
                        # print "new: ", new_fitness, "\n---\n"

                        if new_fitness < self.fitness:
                            self.__switch_genes(i, j)
                            self.fitness = new_fitness
                            return True
        # print("...LS: new fitness: %d " % self.fitness)
        return False

    @staticmethod
    def calc_fitness(lst):
        fitness = 0
        for i, j in enumerate(lst):
            fitness += Cleaner.matrix[i][j]
        return fitness

    def __smart_calc_fitness(self, i, j):
        new_fitness = self.fitness - Cleaner.matrix[i][self.genes[i]]
        new_fitness -= Cleaner.matrix[j][self.genes[j]]
        new_fitness += Cleaner.matrix[i][self.genes[j]]
        new_fitness += Cleaner.matrix[j][self.genes[i]]
        return new_fitness

    def mutation(self, i, j):
        new_fitness = self.__smart_calc_fitness(i, j)
        self.__switch_genes(i, j)
        self.fitness = new_fitness

    def __switch_genes(self, i, j):
        temp = self.genes[i]
        self.genes[i] = self.genes[j]
        self.genes[j] = temp





