Istructions:

1) cd <Minizinc_PATH>
2) ./mzn2fzn <FILE>.mzn  <DATA_FILE>.dzn
3) ./fzn-gecode -a -s -p <#processing_units> <OUT_FILE>.fzn 2>&1 | python3 data_beautifier.py

example:

1) cd Downloads/MiniZincIDE-2.0.13-bundle-linux-x86_64/
2) ./mzn2fzn /home/heis/Dropbox/AI/Progetto/Constraint_Programming/dieta.mzn  /home/heis/Dropbox/AI/Progetto/Constraint_Programming/dieta_all.dzn
3) ./fzn-gecode -a -s -p 8 /home/heis/Dropbox/AI/Progetto/Constraint_Programming/dieta.fzn 2>&1 | python3 /home/heis/Dropbox/AI/Progetto/Constraint_Programming/data_beautifier.py
