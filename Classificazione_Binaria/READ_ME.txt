
/*
* tested on LINUX mint
*/
1) open terminal 
2) p=1 && n=1
3) run command replaceing the Randomize's input with the initial data set;
4) p=1 && n=2
5) run #command_1 appending '&& ((p++)) && ((n++))'
6) repeat number 5) many times as you want

#command_1:
java -cp weka.jar weka.filters.unsupervised.instance.Randomize -S 42 -i <PREV_RND_SET>$p.csv -o <NEW_RND_SET>$n.csv && java -cp weka.jar weka.core.converters.CSVSaver -i <NEW_RND_SET>$n.csv -o <NEW_RND_SET>$n.csv && java -cp weka.jar weka.filters.unsupervised.instance.RemovePercentage -P 30.0 -i <NEW_RND_SET>$n.csv -o <TRAINING_SET>$n.csv && java -cp weka.jar weka.filters.unsupervised.instance.RemovePercentage -P 30.0 -V -i <NEW_RND_SET>$n.csv -o <TEST_SET>$n.csv

//converts sets into csv files; (lets say you have training and test sets stored in different folders)
7) for i in test_sets/*.csv; do java -cp weka.jar weka.core.converters.CSVSaver -i "$i" -o "$i"; done
8) for i in training_sets/*.csv; do java -cp weka.jar weka.core.converters.CSVSaver -i "$i" -o "$i"; done

// run the tests
9) n=1 //will run the test with trainingSet1 and test1
10) run #command_2
11) got to 9) and repeat foreach pair of trainingSet and testSet (remember to increment n)

#command_2:
java -classpath weka.jar:/home/<USER>/wekafiles/packages/LibSVM/LibSVM.jar:/home/<USER>/wekafiles/packages/LibSVM/lib/libsvm.jar weka.classifiers.functions.LibSVM -S 0 -K 2 -D 3 -G 0.0 -R 0.0 -N 0.5 -M 40.0 -C 1.0 -E 0.001 -P 0.1 -model /home/<USER> -seed 1 -output-debug-info -t training_sets/<TRAINING_SET>$n.csv -T test_sets/<TEST_SET>$n.csv




--------------------
Example:

commmand_1:

java -cp weka.jar weka.filters.unsupervised.instance.Randomize -S 42 -i /home/heis/Dropbox/AI/p2/sets/set_rnd_$p.csv -o /home/heis/Dropbox/AI/p2/sets/set_rnd_$n.csv && java -cp weka.jar weka.core.converters.CSVSaver -i /home/heis/Dropbox/AI/p2/sets/set_rnd_$n.csv -o /home/heis/Dropbox/AI/p2/sets/set_rnd_$n.csv && java -cp weka.jar weka.filters.unsupervised.instance.RemovePercentage -P 30.0 -i /home/heis/Dropbox/AI/p2/sets/set_rnd_$n.csv -o /home/heis/Dropbox/AI/p2/training_sets/training$n.csv && java -cp weka.jar weka.filters.unsupervised.instance.RemovePercentage -P 30.0 -V -i /home/heis/Dropbox/AI/p2/sets/set_rnd_$n.csv -o /home/heis/Dropbox/AI/p2/test_sets/test$n.csv

command_2:

java -classpath weka.jar:/home/heis/wekafiles/packages/LibSVM/LibSVM.jar:/home/heis/wekafiles/packages/LibSVM/lib/libsvm.jar weka.classifiers.functions.LibSVM -S 0 -K 2 -D 3 -G 0.0 -R 0.0 -N 0.5 -M 40.0 -C 1.0 -E 0.001 -P 0.1 -model /home/heis -seed 1 -output-debug-info -t /home/heis/Dropbox/AI/p2/training_sets/training$n.csv -T /home/heis/Dropbox/AI/p2/test_sets/test$n.csv
