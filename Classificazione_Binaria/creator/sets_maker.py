#!/usr/bin/python
# get subprocess module
import subprocess
import argparse
import shutil
import os


def create_sets(first_set, iters, r, ptr, pte):
    global wpath
    r = str(r)
    new, prev = 1, 1
    ptr = str(ptr)
    pte = str(pte)
    working_dir = os.getcwd()

    for i in range(iters):
        n, p = str(new), str(prev)
        cmd = "java -cp " + wpath + "/weka.jar weka.filters.unsupervised.instance.Randomize -S " + r + " -i <PREV_RND_SET>.csv -o <NEW_RND_SET>" + n + ".csv " +\
              "&& java -cp " + wpath + "/weka.jar weka.core.converters.CSVSaver -i <NEW_RND_SET>" + n + ".csv -o <NEW_RND_SET>" + n + ".csv " +\
              "&& java -cp " + wpath + "/weka.jar weka.filters.unsupervised.instance.RemovePercentage -P " + ptr + " -i <NEW_RND_SET>" + n + ".csv -o <TRAINING_SET>" + n + ".csv " +\
              "&& java -cp " + wpath + "/weka.jar weka.filters.unsupervised.instance.RemovePercentage -P " + pte + " -V -i <NEW_RND_SET>" + n + ".csv -o <TEST_SET>" + n + ".csv"

        if new == 1:
            cmd = cmd.replace("<PREV_RND_SET>", first_set)
            new += 1
        else:
            cmd = cmd.replace("<PREV_RND_SET>", working_dir + "/sets/set_rnd_" + p)
            prev += 1
            new += 1
        # print prev, new

        cmd = cmd.replace("<NEW_RND_SET>",  working_dir + "/sets/set_rnd_")
        cmd = cmd.replace("<TRAINING_SET>", working_dir + "/training_sets/training")
        cmd = cmd.replace("<TEST_SET>", working_dir + "/test_sets/test")

        print cmd, "\n"

        p = subprocess.Popen(cmd, stdout=subprocess.PIPE, shell=True)
        p.communicate()

        # Wait for date to terminate. Get return returncode ##
        p.wait()
        print "Sets created : ", i+1
        # print "Command exit status/return code : ", p_status

    cmds = ["for i in " + working_dir + "/test_sets/*.csv; do java -cp " + wpath + "/weka.jar weka.core.converters.CSVSaver -i \"$i\" -o \"$i\"; done",
            "for i in " + working_dir + "/training_sets/*.csv; do java -cp " + wpath + "/weka.jar weka.core.converters.CSVSaver -i \"$i\" -o \"$i\"; done"]
    for c in cmds:
        # print c
        p = subprocess.Popen(c, stdout=subprocess.PIPE, shell=True)
        p.communicate()
        p.wait()


def svm(c, k, num_file, exception):
    c = str(c)
    k = str(k)
    num_file = str(num_file)
    global wpath
    working_dir = os.getcwd()

    weka_exception = "java -classpath " + wpath + "/weka.jar weka.classifiers.misc.InputMappedClassifier " +\
                     "-I -trim -t " + working_dir + "/training_sets/training" + num_file + ".csv -T " + working_dir + "/test_sets/test" + num_file + ".csv -W " +\
                     "weka.classifiers.functions.LibSVM -- -S 0 -K " + k + " -D 3 -G 0.0 -R 0.0 -N 0.5 -M 40.0 -C " + c + " -E 0.001 -P 0.1 -model /home/heis -seed 1 -output-debug-info"

    cmd = "java -classpath " + wpath + "/weka.jar:/home/heis/wekafiles/packages/LibSVM/LibSVM.jar:/home/heis/wekafiles/packages/LibSVM/lib/libsvm.jar " +\
        "weka.classifiers.functions.LibSVM -S 0 -K " + k + " -D 3 -G 0.0 -R 0.0 -N 0.5 -M 40.0 -C " + c + " -E 0.001 -P 0.1 -model /home/heis -seed 1 -output-debug-info -t " +\
        working_dir + "/training_sets/training" + num_file + ".csv -T " + working_dir + "/test_sets/test" + num_file + ".csv"

    if exception:
        cmd = weka_exception
    print cmd

    p = subprocess.Popen(cmd, stdout=subprocess.PIPE, shell=True)
    (output, err) = p.communicate()
    p.wait()

    if '' == output:
        print svm(c, k, num_file, True)
    print output


def perceptron(lr, num_file, exception):
    lr = str(lr)
    num_file = str(num_file)
    global wpath
    working_dir = os.getcwd()

    weka_exception = "java -classpath " + wpath + "/weka.jar weka.classifiers.misc.InputMappedClassifier " +\
                     "-I -trim -t " + working_dir + "/training_sets/training" + num_file + ".csv -T " + working_dir + "/test_sets/test" + num_file + ".csv -W " +\
                     "weka.classifiers.functions.MultilayerPerceptron -- -L " + lr + " -M 0.2 -N 500 -V 0 -S 0 -E 20 -H a"

    cmd = "java -classpath " + wpath + "/weka.jar " +\
          "weka.classifiers.functions.MultilayerPerceptron -L " + lr + " -M 0.2 -N 500 -V 0 -S 0 -E 20 -H a -t " +\
          working_dir + "/training_sets/training" + num_file + ".csv -T " + working_dir + "/test_sets/test" + num_file + ".csv"

    if exception:
        cmd = weka_exception
    print cmd

    p = subprocess.Popen(cmd, stdout=subprocess.PIPE, shell=True)
    (output, err) = p.communicate()
    p.wait()

    if '' == output:
        print perceptron(lr, num_file, True)
    print "----", output


if __name__ == '__main__':

    parser = argparse.ArgumentParser(description='App')
    parser.add_argument('-wpath', metavar='WEKA_JAR_PATH', type=str, required=True, help='weka jar absolute path')
    subparsers = parser.add_subparsers(help='help for subcommand')

    maker_parser = subparsers.add_parser('MAKER', add_help=True)
    maker_parser.add_argument('-f', metavar='FILE', type=str, required=True, help='input set file')
    maker_parser.add_argument('-r', metavar='SEED', type=int, required=True, help='[int] seed used to generate a random number used for shuffling')
    maker_parser.add_argument('-rmPercentageTraining', metavar='P_TRAINING', type=float, required=True, help='[float] Remove the given percentage and create a training set with the remaining data')
    maker_parser.add_argument('-rmPercentageTest', metavar='P_TEST', type=float, required=True, help='[float] Keeps the given percentage and create a test set with the remaining data')
    maker_parser.add_argument('-n', metavar='N_SETS', type=int, required=True, help='number of output sets')

    parent_parser = argparse.ArgumentParser(add_help=False)
    # parent_parser.add_argument('-wpath', metavar='WEKA_JAR_PATH', type=str, required=True, help='weka jar absolute path')
    parent_parser.add_argument('-nfile', metavar='N_FILE', type=str, required=True, help='[int] run svm algorithm using test<n> and training<n>')

    parser_a = subparsers.add_parser('SVM', parents=[parent_parser], add_help=True)
    # parser_a.add_argument('-wpath', metavar='WEKA_JAR_PATH', type=str, required=True, help='weka jar absolute path')
    # parser_a.add_argument('-nfile', metavar='N_FILE', type=str, required=True, help='[int] run svm algorithm using test<n> and training<n>')
    parser_a.add_argument('-C', metavar='COST', type=float, required=False, help='[float] cost assigned to an outsider (default=1.0)')
    parser_a.add_argument('-K', metavar='KERNEL', type=int, required=False, help='0=Linear; 1=Polynomial; 2=radial_basis; 3=sigmoid; (default=2)')

    parser_b = subparsers.add_parser('PERC', parents=[parent_parser], add_help=True)
    # parser_b.add_argument('-wpath', metavar='WEKA_JAR_PATH', type=str, required=True, help='weka jar absolute path')
    # parser_b.add_argument('-nfile', metavar='N_FILE', type=str, required=True, help='[int] run svm algorithm using test<n> and training<n>')
    parser_b.add_argument('-LR', metavar='LEARNING_RATE', type=float, required=False, help='[float] number (default 0.3)')

    args = parser.parse_args()

    wpath = args.wpath

    if hasattr(args, 'f'):
        istance_file = args.f
        rnd = int(args.r)
        rmPercentageTraining = float(args.rmPercentageTraining)
        rmPercentageTest = float(args.rmPercentageTest)
        num = int(args.n)

        if os.path.exists("sets"):
            shutil.rmtree("sets")
            shutil.rmtree("test_sets")
            shutil.rmtree("training_sets")

        os.mkdir("sets")
        os.mkdir("test_sets")
        os.mkdir("training_sets")

        create_sets(istance_file, num, rnd, rmPercentageTraining, rmPercentageTest)

    elif hasattr(args, 'C'):
        cost = float(args.C)
        kernel = int(args.K)
        nfile = int(args.nfile)
        svm(cost, kernel, nfile, False)
        exit(0)

    else:
        l_rate = float(args.LR)
        nfile = int(args.nfile)
        perceptron(l_rate, nfile, False)
        exit(0)
